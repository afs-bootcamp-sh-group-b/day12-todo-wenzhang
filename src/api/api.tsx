import axios from "axios";
const api = axios.create({
  //baseURL: "https://64c9f061b2980cec85c29003.mockapi.io/",
  baseURL: "http://localhost:8080/",
  timeout: 5000,
});

export default api;
