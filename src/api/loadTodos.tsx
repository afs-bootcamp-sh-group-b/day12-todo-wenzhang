import { Dispatch } from "redux";
import { getAll } from "./todosAPI";

export async function loadTodos(type: string, dispatch: Dispatch) {
  try {
    const response = await getAll();
    dispatch({ type, payload: response.data });
  } catch (error) {
    console.error(error);
  }
}
