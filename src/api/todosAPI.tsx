import { Todo } from "../types/todo";
import api from "./api";

const getAll = async () => {
  return await api.get("todo");
};

const addTodo = async (data: Todo) => {
  return await api.post("todo", data);
};

const updateTodo = async (id: string, data: Todo) => {
  return await api.put(`todo/${id}`, data);
};

const deleteTodo = async (id: string) => {
  return await api.delete(`/todo/${id}`);
};

export { getAll, addTodo, updateTodo, deleteTodo };
