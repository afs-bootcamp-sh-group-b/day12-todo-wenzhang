import React from "react";
import TodoList from "./components/TodoList";
import "./App.css";

const App: React.FC = () => {
  return (
    <div>
      <div className="todoContainer">
        <TodoList />
      </div>
    </div>
  );
};

export default App;
