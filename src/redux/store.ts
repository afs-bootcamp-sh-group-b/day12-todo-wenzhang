import { createSlice, configureStore, PayloadAction } from "@reduxjs/toolkit";
import { Todo } from "../types/todo";
import { addTodo as apiAddTodo } from "../api/todosAPI";

interface State {
  todos: Todo[];
}

const initialState: State = {
  todos: [
    /*
    {
      id: new Date().getTime().toString() + Math.random().toString(),
      text: "todo example",
      done: false,
    },
    {
      id: new Date().getTime().toString() + Math.random().toString(),
      text: "new item",
      done: true,
    },
    {
      id: new Date().getTime().toString() + Math.random().toString(),
      text: "todo example",
      done: false,
    }, */
  ],
};

const todosSlice = createSlice({
  name: "todos",
  initialState,
  reducers: {
    addTodo: (state, action: PayloadAction<Todo>) => {
      state.todos.push(action.payload);
    },
    updateTodoList: (state, action: PayloadAction<Todo[]>) => {
      state.todos = action.payload;
    },
    toggleTodo: (state, action: PayloadAction<string>) => {
      const todo = state.todos.find((todo) => todo.id === action.payload);
      if (todo) {
        todo.done = !todo.done;
      }
    },
    loadAll: (state, action: PayloadAction<Todo[]>) => {
      state.todos = action.payload;
    },
  },
});

const store = configureStore({
  reducer: {
    todos: todosSlice.reducer,
  },
});

export default store;

export type RootState = ReturnType<typeof store.getState>;
