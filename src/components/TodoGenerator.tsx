interface TodoGeneratorProps {
  inputText: string;
  handleInputChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  handleAddTodo: () => void;
}

const TodoGenerator: React.FC<TodoGeneratorProps> = ({
  inputText,
  handleInputChange,
  handleAddTodo,
}) => {
  return (
    <span>
      <input
        type="text"
        value={inputText}
        onChange={handleInputChange}
        placeholder="Input a new todo here..."
      />
      <button onClick={handleAddTodo}>Add</button>
    </span>
  );
};

export default TodoGenerator;
