import { Todo } from "../types/todo";
import TodoItem from "./TodoItem";
import "../styles/TodoGroup.css";

interface TodoGroupProps {
  todos: Todo[];
  onTodoDelete: (id: string) => void;
  onTodoToggle: (id: string) => void;
}

const TodoGroup: React.FC<TodoGroupProps> = ({
  todos,
  onTodoDelete,
  onTodoToggle,
}) => {
  return (
    <div className="todo-group">
      {todos.map((todo) => (
        <TodoItem
          key={todo.id}
          todo={todo}
          onTodoDelete={onTodoDelete}
          onTodoToggle={onTodoToggle}
        />
      ))}
    </div>
  );
};

export default TodoGroup;
