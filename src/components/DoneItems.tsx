import { useSelector } from "react-redux";
import { RootState } from "../redux/store";
import "../styles/DoneItems.css"

const Doneitems: React.FC = () => {
  const todoState = useSelector((state: RootState) => state.todos);
  const todoList = todoState.todos;

  return todoList.filter((todo) => todo.done).length ? (
    <div>
      {todoList.map(
        (todo, index) => todo.done && <h4 className="title" key={index}>{todo.text}</h4>
      )}
    </div>
  ) : (
    <h4 className="title">Sorry, no todo done yet.</h4>
  );
};

export default Doneitems;
