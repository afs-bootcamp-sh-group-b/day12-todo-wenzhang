import React, { useState } from "react";
import { Todo } from "../types/todo";
import "../styles/TodoItem.css";
import EditOutlined from "@ant-design/icons/lib/icons/EditOutlined";
import { Modal, Input } from "antd";
import { updateTodo } from "../api/todosAPI";
import { loadTodos } from "../api/loadTodos";
import { useDispatch } from "react-redux";

interface TodoItemProps {
  todo: Todo;
  onTodoDelete: (id: string) => void;
  onTodoToggle: (id: string) => void;
}

const TodoItem: React.FC<TodoItemProps> = ({
  todo,
  onTodoDelete,
  onTodoToggle,
}) => {
  const { TextArea } = Input;
  const dispatch = useDispatch();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [modalInput, setModalInput] = useState("");

  const handleModalInputChange = (e: {
    target: { value: React.SetStateAction<string> };
  }) => {
    setModalInput(e.target.value);
  };

  const showModal = (e: { stopPropagation: () => void }) => {
    e.stopPropagation();
    setIsModalOpen(true);
  };

  const handleModalOk = async () => {
    setIsModalOpen(false);

    if (modalInput.trim() !== "") {
      const newTodo: Todo = {
        id: todo.id,
        text: modalInput.trim(),
        done: todo.done,
      };
      try {
        const msg = await updateTodo(todo.id, newTodo);
        msg && loadTodos("todos/loadAll", dispatch);
        setModalInput("");
        console.log(msg);
      } catch (e) {
        console.error(e);
      }
    }
  };

  const handleModalCancel = () => {
    setIsModalOpen(false);
  };

  const handleDelete = () => {
    onTodoDelete(todo.id);
  };

  const handleToggle = () => {
    onTodoToggle(todo.id);
  };

  return (
    <div className={"todo-item"} onClick={handleToggle}>
      <span className={`${todo.done ? "done" : ""}`}>{todo.text}</span>
      <div className="actions-container">
        <button className="delete-btn" onClick={handleDelete}>
          ×
        </button>
        <EditOutlined className="delete-btn" onClick={showModal} />
      </div>

      <Modal
        title="Edit todo"
        open={isModalOpen}
        onOk={handleModalOk}
        onCancel={handleModalCancel}
      >
        <TextArea
          rows={3}
          value={modalInput}
          onChange={handleModalInputChange}
          placeholder={`${todo.text}`}
        />
      </Modal>
    </div>
  );
};

export default TodoItem;
