import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Todo } from "../types/todo";
import { RootState } from "../redux/store";
import TodoGroup from "./TodoGroup";
import TodoGenerator from "./TodoGenerator";
import { addTodo, deleteTodo, updateTodo } from "../api/todosAPI";
import { loadTodos } from "../api/loadTodos";
import "../styles/TodoList.css";

const TodoList: React.FC = () => {
  const [inputText, setInputText] = useState<string>("");
  const todoState = useSelector((state: RootState) => state.todos);
  const todoList = todoState.todos;
  const dispatch = useDispatch();

  useEffect(() => {
    loadTodos("todos/loadAll", dispatch);
  }, []);

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setInputText(event.target.value);
  };

  const handleAddTodo = async () => {
    if (inputText.trim() !== "") {
      const newTodo: Todo = {
        id: new Date().getTime().toString(),
        text: inputText.trim(),
        done: false,
      };
      try {
        const response = await addTodo(newTodo);
        dispatch({ type: "todos/addTodo", payload: response.data });
        loadTodos("todos/loadAll", dispatch);
        setInputText("");
      } catch (e) {
        console.error(e);
      }
    }
  };

  const handleTodoDelete = async (id: string) => {
    try {
      const updatedTodoList = todoList.filter((todo) => todo.id !== id);
      (await deleteTodo(id)) &&
        dispatch({ type: "todos/updateTodoList", payload: updatedTodoList });
      loadTodos("todos/loadAll", dispatch);
    } catch (e) {
      console.error(e);
    }
  };

  const handleTodoToggle = async (id: string) => {
    try {
      const todoToUpdate = todoList.find((todo) => todo.id === id);
      if (todoToUpdate) {
        const updatedTodo = { ...todoToUpdate, done: !todoToUpdate.done };
        (await updateTodo(id, updatedTodo)) &&
          dispatch({ type: "todos/toggleTodo", payload: id });
        loadTodos("todos/loadAll", dispatch);
      }
    } catch (e) {
      console.error(e);
    }
  };

  return (
    <div className="todo-container">
      <h1 className="title">TodoList</h1>
      <TodoGroup
        todos={todoList}
        onTodoDelete={handleTodoDelete}
        onTodoToggle={handleTodoToggle}
      />
      <TodoGenerator
        inputText={inputText}
        handleInputChange={handleInputChange}
        handleAddTodo={handleAddTodo}
      />
    </div>
  );
};

export default TodoList;
