import { Link, Outlet } from "react-router-dom";
import { Layout, Menu } from "antd";
import {
  HomeOutlined,
  QuestionCircleOutlined,
  CheckCircleOutlined,
} from "@ant-design/icons";
const { Content } = Layout;

const CustomLayout: React.FC = () => {
  return (
    <Layout className="layout">
      <Menu theme="dark" mode="horizontal" defaultSelectedKeys={["1"]}>
        <Menu.Item key="1" icon={<HomeOutlined />}>
          <Link to="/">Home</Link>
        </Menu.Item>
        <Menu.Item key="2" icon={<QuestionCircleOutlined />}>
          <Link to="/help">Help</Link>
        </Menu.Item>
        <Menu.Item key="3" icon={<CheckCircleOutlined />}>
          <Link to="/done">Done</Link>
        </Menu.Item>
      </Menu>
      <Content style={{ padding: "0 50px" }}>
        <div className="site-layout-content">
          <Outlet></Outlet>
        </div>
      </Content>
    </Layout>
  );
};

export default CustomLayout;
