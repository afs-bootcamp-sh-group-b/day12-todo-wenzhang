import { createBrowserRouter } from "react-router-dom";
import Layout from "../components/Layout";
import Help from "../components/Help";
import TodoList from "../components/TodoList";
import Doneitems from "../components/DoneItems";

export const router = createBrowserRouter([
    {
        path:"/",
        element: <Layout />,
        children: [{
                path:"",
                element: <TodoList />
            },
            {
                path:"help",
                element: <Help />
            },
            {
                path:"done",
                element: <Doneitems />
            },
        ],
        errorElement: <div><h1>你进入了不存在的页面……</h1><h2>大侠请重新来过</h2></div>
    }
])